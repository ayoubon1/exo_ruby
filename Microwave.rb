=begin
class Microwave
  def initialize(buttons)
    @buttons = buttons
  end

  def timer
    minutes = @buttons / 60
    seconds = @buttons % 60
    "#{format('%02d', minutes)}:#{format('%02d', seconds)}"
  end
end

microwave = Microwave.new(70)
puts microwave.timer # Output: "01:59"
=end