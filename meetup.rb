=begin
require 'date'

class Meetup
  DAYS_OF_WEEK = %w[sunday monday tuesday wednesday thursday friday saturday].freeze
  TEENTHS = %w[teenth].freeze
  ORDINALS = %w[first second third fourth fifth last].freeze

  def initialize(month, year)
    @month = month
    @year = year
  end

  def day(day_of_week, descriptor)
    start_date = Date.new(@year, @month, 1)
    end_date = (start_date >> 1) - 1
    if TEENTHS.include?(descriptor)
      meetup_date = find_teenth(start_date, end_date, day_of_week)
    else
      ordinal = ORDINALS.index(descriptor) + 1
      meetup_date = find_ordinal(start_date, end_date, day_of_week, ordinal)
    end
    meetup_date
  end

  private

  def find_teenth(start_date, end_date, day_of_week)
    (start_date..end_date).find do |date|
      date.day >= 13 && DAYS_OF_WEEK[date.wday] == day_of_week
    end
  end

  def find_ordinal(start_date, end_date, day_of_week, ordinal)
    current_week = -1
    (start_date..end_date).each do |date|
      current_week += 1 if DAYS_OF_WEEK[start_date.wday] == day_of_week
      return date if current_week == ordinal - 1 && DAYS_OF_WEEK[date.wday] == day_of_week
    end
  end
end
meetup = Meetup.new(1, 2023)
puts meetup.day('Monday', 'third')
=end