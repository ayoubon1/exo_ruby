class SimpleCalculator
        def self.calculate(a, b, operation)
        raise ArgumentError, "Invalid argument types" unless a.is_a?(Numeric) && b.is_a?(Numeric)
        raise UnsupportedOperation, "Unsupported operation" unless %w(+ * /).include?(operation)
        
        result = a.public_send(operation, b)
        raise ZeroDivisionError, "Division by zero is not allowed." if operation == "/" && b.zero?
        begin
        "#{a} #{operation} #{b} = #{result}"
        rescue ZeroDivisionError
        "Division by zero is not allowed."
        end
        end
        ALLOWED_OPERATIONS = ['+', '/', '*'].freeze
        private_constant :ALLOWED_OPERATIONS
      end